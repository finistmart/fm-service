#FinistMart Service

## Build

```
mvn package
```

## Database

### MySQL

Login to database:

```shell script
mysql -uroot -p
```

Create database and user:

```sql
CREATE DATABASE finistmart DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON finistmart.* TO 'finistmart'@'localhost' identified by "finistmart";
```

Run migrations to create database and insert mandatorary data:

```shell script
mvn liquibase:update -Denv=postgres
```

Ensure database.properties has the correct definition

```
production.driver=com.mysql.jdbc.Driver
production.username=finistmart
production.password=finistmart
production.url=jdbc:mysql://5.9.81.230/finistmart?useUnicode=yes&characterEncoding=UTF-8
```

and links in spring-hibernate-jersey2.xml are pointed to it:
 
 
```
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="${development.driver}" />
        <property name="url" value="${development.url}" />
        <property name="username" value="${development.username}" />
        <property name="password" value="${development.password}" />
    </bean>
```


### PostgreSQL

Login to database:

```shell script
sudo -u postgres psql
```

Create database and user:

```postgresql
create database finistmart;
create user finistmart with encrypted password 'finistmart';
grant all privileges on database finistmart to finistmart;
```

Run migrations to create database and insert mandatorary data:

```shell script
mvn liquibase:update -Denv=mysql
```

Ensure database.properties has the correct definition 

```
development.driver=org.postgresql.Driver
development.username=finistmart
development.password=finistmart
development.url=jdbc:postgresql://localhost/finistmart
```

and links in spring-hibernate-jersey2.xml are pointed to it:

```
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="${development.driver}" />
        <property name="url" value="${development.url}" />
        <property name="username" value="${development.username}" />
        <property name="password" value="${development.password}" />
    </bean>
```


## Deploy

for Tomcat copy target/fm-service.war to webapps/