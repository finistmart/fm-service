package com.finistmart.dto;

public class OrdersAdd {

    private String data;

    private String buyerIdent;

    private String appIdent;

    public OrdersAdd() {
    }

    public OrdersAdd(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getBuyerIdent() {
        return buyerIdent;
    }

    public void setBuyerIdent(String buyerIdent) {
        this.buyerIdent = buyerIdent;
    }

    public String getAppIdent() {
        return appIdent;
    }

    public void setAppIdent(String appIdent) {
        this.appIdent = appIdent;
    }
}
