package com.finistmart;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;


public class FmServiceApplication extends ResourceConfig
{
    public FmServiceApplication()
    {
        packages("com.finistmart.webservice");
        register(LoggingFilter.class);
        //register(GsonMessageBodyHandler.class);
 
        //Register Auth Filter here
        register(AuthenticationFilter.class);
    }
}