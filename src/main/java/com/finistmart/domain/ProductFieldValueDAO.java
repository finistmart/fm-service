package com.finistmart.domain;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("productFieldValueDAO")
public class ProductFieldValueDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @CacheEvict(value={"productfieldvalues", "products"}, allEntries = true)
    public ProductFieldValue create(ProductFieldValue productFieldValue) {
        Long productFieldValueId = (Long) sessionFactory.getCurrentSession().save(productFieldValue);
        return get(productFieldValueId);
    }

    @Cacheable(value="productfieldvalues", key="#id")
    public ProductFieldValue get(Long id) {
        ProductFieldValue productFieldValue = (ProductFieldValue) sessionFactory.getCurrentSession().get(ProductFieldValue.class, id);
        return productFieldValue;
    }

    @CacheEvict(value={"productfieldvalues", "products"}, allEntries = true)
    public void update(ProductFieldValue productFieldValue) {
        sessionFactory.getCurrentSession().update(productFieldValue);
    }

    @CacheEvict(value={"productfieldvalues", "products"}, allEntries = true)
    public void remove(ProductFieldValue productFieldValue) {
        sessionFactory.getCurrentSession().delete(productFieldValue);
    }
}