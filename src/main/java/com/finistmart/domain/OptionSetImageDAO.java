package com.finistmart.domain;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository("optionSetImageDAO")
public class OptionSetImageDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @CacheEvict(value={"optionsetimages", "optionsets", "products"}, allEntries = true)
    public OptionSetImage create(OptionSetImage optionSetImage) {
        Long optionSetImageId = (Long) sessionFactory.getCurrentSession().save(optionSetImage);
        return get(optionSetImageId);
    }

    @Cacheable(value="optionsetimages", key="#id")
    public OptionSetImage get(Long id) {
        OptionSetImage optionSetImage = (OptionSetImage) sessionFactory.getCurrentSession().get(OptionSetImage.class, id);
        return optionSetImage;
    }

    @CacheEvict(value={"optionsetimages", "optionsets", "products"}, allEntries = true)
    public void update(OptionSetImage optionSetImage) {
        sessionFactory.getCurrentSession().save(optionSetImage);
    }

    @CacheEvict(value={"optionsetimages", "optionsets", "products"}, allEntries = true)
    public void delete(OptionSetImage optionSetImage) {
        sessionFactory.getCurrentSession().delete(optionSetImage);
    }
}