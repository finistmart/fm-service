package com.finistmart.domain;




import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Cacheable
@Entity
@Table(name="products")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="name")
    private String name;

    @ManyToMany
    @JoinTable(
            name = "products_categories",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    private List<ProductFieldValue> fieldValues;

    @OneToMany(mappedBy = "product")
    private List<ProductOptionSet> optionSets;

    @OneToOne(mappedBy = "product", fetch = FetchType.LAZY)
    private ProductImage image;

    @ManyToMany
    @JoinTable(
            name = "products_showcases",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "showcase_id"))
    private List<ShowCase> showCases;

    public Product() {
    }

    public Product(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    // getters & setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<ShowCase> getShowCases() {
        return showCases;
    }

    public void setShowCases(List<ShowCase> showCases) {
        this.showCases = showCases;
    }

    public List<ProductFieldValue> getFieldValues() {
        // sort by weight asc
        Collections.sort(fieldValues, (v1, v2) -> - v1.getInfoField().getWeight() - v2.getInfoField().getWeight() );
        return fieldValues;
    }

    public void setFieldValues(List<ProductFieldValue> fieldValues) {
        this.fieldValues = fieldValues;
    }

    public List<ProductOptionSet> getOptionSets() {
        return optionSets;
    }

    public void setOptionSets(List<ProductOptionSet> optionSets) {
        this.optionSets = optionSets;
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }
}
