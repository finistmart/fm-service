package com.finistmart.domain;


import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Cacheable
@Entity
@Table(name="orders")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Lob
    @Column(name="data", columnDefinition="TEXT")
    private String data;

    @Lob
    @Column(name="notes", columnDefinition="TEXT")
    private String notes;

    private String status;

    private LocalDateTime created;

    private LocalDateTime updated;

    @Column(name="buyer_ident")
    private String buyerIdent;

    @Column(name="app_ident")
    private String appIdent;

    @Column(name="app_type")
    private String appType;

    public Order() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public String getBuyerIdent() {
        return buyerIdent;
    }

    public void setBuyerIdent(String buyerIdent) {
        this.buyerIdent = buyerIdent;
    }

    public String getAppIdent() {
        return appIdent;
    }

    public void setAppIdent(String appIdent) {
        this.appIdent = appIdent;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }
}
