package com.finistmart.domain;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Cacheable
@Entity
@Table(name="optionsetimages")
public class OptionSetImage implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name="file_name", nullable = false)
    private String fileName;

    @Column(name="mime_type")
    private String mimeType;

    @Column(name="image_data", columnDefinition = "text")
    private String imageData;

    @JsonbTransient
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "optionset_id")
    private ProductOptionSet optionSet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public ProductOptionSet getOptionSet() {
        return optionSet;
    }

    public void setOptionSet(ProductOptionSet optionSet) {
        this.optionSet = optionSet;
    }
}
