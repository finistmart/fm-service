package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Root;

import java.util.List;

@Transactional
@Repository("productDAO")
public class ProductDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Cacheable(value = "products", key="{ #methodName, #categoryId, #pageNum, #pageSize}")
    public List<Product> getByCategoryId(Long categoryId, Integer pageNum, Integer pageSize) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> products = criteriaQuery.from(Product.class);

        CriteriaQuery<Product> select = criteriaQuery.select(products);
        if (categoryId != null) {
            ListJoin<Product, Category> categories = products.join(Product_.categories);

            CriteriaQuery<Long> subcatQuery = criteriaBuilder.createQuery(Long.class);
            Root<Category> subcategories = subcatQuery.from(Category.class);
            subcatQuery.where(criteriaBuilder.equal(subcategories.get("parentId"), categoryId));
            subcatQuery.select(subcategories.get(Category_.id));
            TypedQuery<Long> subcatTQuery = session.createQuery(subcatQuery);
            List<Long> subcatIds = subcatTQuery.getResultList();
            if (subcatIds.size() > 0) {
                select.where(criteriaBuilder.or(
                        criteriaBuilder.equal(categories.get("id"), categoryId),
                        criteriaBuilder.in(categories.get("id")).value(subcatIds)
                ));
            } else {
                select.where(criteriaBuilder.equal(categories.get("id"), categoryId));
            }
        }

        TypedQuery<Product> typedQuery = session.createQuery(select);
        if (pageSize != null) { // paging requested
            typedQuery.setMaxResults(pageSize);
            if (pageNum > 0) {
                typedQuery.setFirstResult((pageNum * pageSize) - pageSize);
            }
        }
        List<Product> productList = typedQuery.getResultList();
        return productList;
    }

    public Long getByCategoryIdCount(Long categoryId) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Product> products = criteriaQuery.from(Product.class);

        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(products.get("id")));
        if (categoryId != null) {
            ListJoin<Product, Category> categories = products.join(Product_.categories);

            CriteriaQuery<Long> subcatQuery = criteriaBuilder.createQuery(Long.class);
            Root<Category> subcategories = subcatQuery.from(Category.class);
            subcatQuery.where(criteriaBuilder.equal(subcategories.get("parentId"), categoryId));
            subcatQuery.select(subcategories.get(Category_.id));
            TypedQuery<Long> subcatTQuery = session.createQuery(subcatQuery);
            List<Long> subcatIds = subcatTQuery.getResultList();
            if (subcatIds.size() > 0) {
                select.where(criteriaBuilder.or(
                        criteriaBuilder.equal(categories.get("id"), categoryId),
                        criteriaBuilder.in(categories.get("id")).value(subcatIds)
                ));
            } else {
                select.where(criteriaBuilder.equal(categories.get("id"), categoryId));
            }
        }

        TypedQuery<Long> typedQuery = session.createQuery(select);

        Long count = typedQuery.getSingleResult();
        return count;
    }

    @CacheEvict(value="products", key="#product.id", allEntries = true)
    public Product create(Product product) {
        Long productId = (Long) sessionFactory.getCurrentSession().save(product);
        return get(productId);
    }

    @Cacheable(value="products", key="#id")
    public Product get(Long id) {
        Product product = (Product) sessionFactory.getCurrentSession().get(Product.class, id);
        return product;
    }

    @CacheEvict(value="products", key="#product.id", allEntries = true)
    public void update(Product product) {
        sessionFactory.getCurrentSession().update(product);
    }

    @CacheEvict(value="products", key="#product.id", allEntries = true)
    public void remove(Product product) {
        sessionFactory.getCurrentSession().delete(product);
    }
}