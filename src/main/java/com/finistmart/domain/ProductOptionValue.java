package com.finistmart.domain;


import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Cacheable
@Entity
@Table(name="productoptionvalues")
public class ProductOptionValue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "productoptionset_id")
    private ProductOptionSet productOptionSet;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "infofield_id")
    private InfoField infoField;

    @Lob
    @Column(name="value", columnDefinition="TEXT")
    private String value;

    public ProductOptionValue() {
    }


    // getters & setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductOptionSet getProductOptionSet() {
        return productOptionSet;
    }

    public void setProductOptionSet(ProductOptionSet productOptionSet) {
        this.productOptionSet = productOptionSet;
    }

    public InfoField getInfoField() {
        return infoField;
    }

    public void setInfoField(InfoField infoField) {
        this.infoField = infoField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
