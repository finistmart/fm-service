package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository("showCaseDAO")
public class ShowCaseDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Cacheable(value="showcases", key="{ #methodName }")
    public List<ShowCase> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<ShowCase> criteriaQuery = criteriaBuilder.createQuery(ShowCase.class);
        Root<ShowCase> from = criteriaQuery.from(ShowCase.class);
        CriteriaQuery<ShowCase> select = criteriaQuery.select(from);
        TypedQuery<ShowCase> typedQuery = session.createQuery(select);
        List<ShowCase> showCases = typedQuery.getResultList();
        return showCases;
    }

    @CacheEvict(value="showcases", key="#showCase.id")
    public ShowCase create(ShowCase showCase) {
        Long id = (Long) sessionFactory.getCurrentSession().save(showCase);
        return get(id);
    }

    @Cacheable(value="showcases", key="#id")
    public ShowCase get(Long id) {
        ShowCase showCase = (ShowCase) sessionFactory.getCurrentSession().get(ShowCase.class, id);
        return showCase;
    }

    @CacheEvict(value="showcases", key="#showCase.id")
    public void update(ShowCase showCase) {
        sessionFactory.getCurrentSession().update(showCase);
    }

    @CacheEvict(value="showcases", key="#showCase.id")
    public void remove(ShowCase showCase) {
        sessionFactory.getCurrentSession().delete(showCase);
    }
}