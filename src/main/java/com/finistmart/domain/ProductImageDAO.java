package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository("productImageDAO")
public class ProductImageDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Cacheable(value="productimages", key="{ #methodName, #categoryId}")
    public List<ProductImage> getAll(Long categoryId) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<ProductImage> criteriaQuery = criteriaBuilder.createQuery(ProductImage.class);
        Root<ProductImage> productImages = criteriaQuery.from(ProductImage.class);

        CriteriaQuery<ProductImage> select = criteriaQuery.select(productImages);
        TypedQuery<ProductImage> typedQuery = session.createQuery(select);
        List<ProductImage> productImageList = typedQuery.getResultList();
        return productImageList;
    }

    @Cacheable(value="productimages", key="#productImage.id")
    public ProductImage create(ProductImage productImage) {
        Long productImageId = (Long) sessionFactory.getCurrentSession().save(productImage);
        return get(productImageId);
    }

    @Cacheable(value="productimages", key="#id")
    public ProductImage get(Long id) {
        ProductImage productImage = (ProductImage) sessionFactory.getCurrentSession().get(ProductImage.class, id);
        return productImage;
    }

    @CacheEvict(value={"productimages", "productimages_bycategory", "products"}, allEntries = true)
    public ProductImage update(ProductImage productImage) {
        sessionFactory.getCurrentSession().save(productImage);
        return get(productImage.getId());
    }

    @CacheEvict(value={"productimages", "productimages_bycategory", "products"}, allEntries = true)
    public void remove(ProductImage productImage) {
        sessionFactory.getCurrentSession().delete(productImage);
    }
}