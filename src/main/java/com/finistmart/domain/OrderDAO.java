package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository("orderDAO")
public class OrderDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Cacheable(value="orders", key="{ #methodName, #showCaseId, #appIdent }")
    public List<Order> getAll(Long showCaseId, String appIdent) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Order> criteriaQuery = criteriaBuilder.createQuery(Order.class);
        Root<Order> orders = criteriaQuery.from(Order.class);

        CriteriaQuery<Order> select = criteriaQuery.select(orders);

        if (appIdent != null) {
            select.where(criteriaBuilder.equal(orders.get("appIdent"), appIdent));
        }

        TypedQuery<Order> typedQuery = session.createQuery(select);

        List<Order> orderList = typedQuery.getResultList();
        return orderList;
    }

    @CacheEvict(value="orders", allEntries = true)
    public Order create(Order order) {
        Long id = (Long) sessionFactory.getCurrentSession().save(order);
        return get(id);
    }

    @Cacheable(value="orders", key="#id")
    public Order get(Long id) {
        Order order = (Order) sessionFactory.getCurrentSession().get(Order.class, id);
        return order;
    }

    @CacheEvict(value="orders", allEntries = true)
    public void update(Order order) {
        sessionFactory.getCurrentSession().update(order);
    }

    @CacheEvict(value="orders", allEntries = true)
    public void remove(Order order) {
        sessionFactory.getCurrentSession().delete(order);
    }
}