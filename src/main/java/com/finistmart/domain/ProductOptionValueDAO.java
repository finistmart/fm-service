package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository("productOptionValueDAO")
public class ProductOptionValueDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Cacheable(value="productoptionvalues", key="{ #methodName, #productId}")
    public List<ProductOptionValue> getByProductId(Long productId) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<ProductOptionValue> criteriaQuery = criteriaBuilder.createQuery(ProductOptionValue.class);
        Root<ProductOptionValue> optionValues = criteriaQuery.from(ProductOptionValue.class);

        CriteriaQuery<ProductOptionValue> select = criteriaQuery.select(optionValues);
        if (productId != null) {
            select.where(criteriaBuilder.equal(optionValues.get("productOptionSet").get("productOption").get("id"), productId));
        }
        TypedQuery<ProductOptionValue> typedQuery = session.createQuery(select);
        List<ProductOptionValue> optionSetList = typedQuery.getResultList();
        return optionSetList;
    }

    @Cacheable(value="productoptionvalues", key="{ #methodName, #optionSetId}")
    public List<ProductOptionValue> getByOptionSet(Long optionSetId) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<ProductOptionValue> criteriaQuery = criteriaBuilder.createQuery(ProductOptionValue.class);
        Root<ProductOptionValue> optionValues = criteriaQuery.from(ProductOptionValue.class);

        CriteriaQuery<ProductOptionValue> select = criteriaQuery.select(optionValues);
        if (optionSetId != null) {
            select.where(criteriaBuilder.equal(optionValues.get("productOptionSet").get("id"), optionSetId));
        }
        TypedQuery<ProductOptionValue> typedQuery = session.createQuery(select);
        List<ProductOptionValue> optionSetList = typedQuery.getResultList();
        return optionSetList;
    }

    @CacheEvict(value={"productoptionvalues", "productoptionsets"}, allEntries = true)
    public ProductOptionValue create(ProductOptionValue productOptionValue) {
        Long productOptionValueId = (Long) sessionFactory.getCurrentSession().save(productOptionValue);
        return get(productOptionValueId);
    }

    @Cacheable(value="productoptionvalues", key="{ #methodName, #id }")
    public ProductOptionValue get(Long id) {
        ProductOptionValue productOptionValue = (ProductOptionValue) sessionFactory.getCurrentSession().get(ProductOptionValue.class, id);
        return productOptionValue;
    }

    @CacheEvict(value={"productoptionvalues", "productoptionsets"}, allEntries = true)
    public void update(ProductOptionValue productOptionValue) {
        sessionFactory.getCurrentSession().update(productOptionValue);
    }

    @CacheEvict(value={"productoptionvalues", "productoptionsets"}, allEntries = true)
    public void remove(ProductOptionValue productOptionValue) {
        sessionFactory.getCurrentSession().delete(productOptionValue);
    }
}