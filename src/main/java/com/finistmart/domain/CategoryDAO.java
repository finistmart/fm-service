package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository("categoryDAO")
public class CategoryDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Cacheable(value="categories", key="{ #methodName }")
    public List<Category> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> from = criteriaQuery.from(Category.class);
        CriteriaQuery<Category> select = criteriaQuery.select(from);
        TypedQuery<Category> typedQuery = session.createQuery(select);
        List<Category> categories = typedQuery.getResultList();
        return categories;
    }

    @CacheEvict(value="categories", allEntries = true)
    public Category create(Category category) {
        Long id = (Long) sessionFactory.getCurrentSession().save(category);
        return get(id);
    }

    @Cacheable(value="categories", key="#id")
    public Category get(Long id) {
        Category category = (Category) sessionFactory.getCurrentSession().get(Category.class, id);
        return category;
    }

    @CacheEvict(value="categories", allEntries = true)
    public void update(Category category) {
        sessionFactory.getCurrentSession().update(category);
    }

    @CacheEvict(value="categories", allEntries = true)
    public void remove(Category category) {
        sessionFactory.getCurrentSession().delete(category);
    }
}