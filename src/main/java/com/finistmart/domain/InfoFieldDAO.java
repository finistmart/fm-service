package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository("infoFieldDAO")
public class InfoFieldDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Cacheable(value="infofields", key="{ #methodName }")
    public List<InfoField> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<InfoField> criteriaQuery = criteriaBuilder.createQuery(InfoField.class);
        Root<InfoField> from = criteriaQuery.from(InfoField.class);
        CriteriaQuery<InfoField> select = criteriaQuery.select(from);
        TypedQuery<InfoField> typedQuery = session.createQuery(select);
        List<InfoField> infoFields = typedQuery.getResultList();
        return infoFields;
    }

    @Cacheable(value="infofields", key="#id")
    public InfoField get(Long id) {
        InfoField infoField = (InfoField) sessionFactory.getCurrentSession().get(InfoField.class, id);
        return infoField;
    }

}