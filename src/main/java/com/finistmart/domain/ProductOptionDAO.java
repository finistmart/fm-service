package com.finistmart.domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository("productOptionDAO")
public class ProductOptionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @SuppressWarnings("unchecked")
    @Cacheable(value="productoptions", key="{ #methodName }")
    public List<ProductOption> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<ProductOption> criteriaQuery = criteriaBuilder.createQuery(ProductOption.class);
        Root<ProductOption> optionsRoot = criteriaQuery.from(ProductOption.class);

        CriteriaQuery<ProductOption> select = criteriaQuery.select(optionsRoot);
        TypedQuery<ProductOption> typedQuery = session.createQuery(select);
        List<ProductOption> options = typedQuery.getResultList();
        return options;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Cacheable(value="productoptions", key="#id")
    public ProductOption get(Long id) {
        ProductOption productOption = (ProductOption) sessionFactory.getCurrentSession().get(ProductOption.class, id);
        return productOption;
    }
}