package com.finistmart.domain;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("productOptionSetDAO")
public class ProductOptionSetDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @CacheEvict(value={"productoptionsets", "products"}, allEntries = true)
    public ProductOptionSet create(ProductOptionSet productOptionSet) {
        Long productOptionSetId = (Long) sessionFactory.getCurrentSession().save(productOptionSet);
        return get(productOptionSetId);
    }

    @Cacheable(value="productoptionsets", key="#id")
    public ProductOptionSet get(Long id) {
        ProductOptionSet productOptionSet = (ProductOptionSet) sessionFactory.getCurrentSession().get(ProductOptionSet.class, id);
        return productOptionSet;
    }

    @CacheEvict(value={"productoptionsets", "products"}, allEntries = true)
    public void update(ProductOptionSet productOptionSet) {
        sessionFactory.getCurrentSession().update(productOptionSet);
    }

    @CacheEvict(value={"productoptionsets", "products"}, allEntries = true)
    public void delete(ProductOptionSet productOptionSet) {
        sessionFactory.getCurrentSession().delete(productOptionSet);
    }
}