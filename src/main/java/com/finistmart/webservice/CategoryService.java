package com.finistmart.webservice;

import com.finistmart.domain.Category;
import com.finistmart.domain.CategoryDAO;

import com.finistmart.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;


@Component
@Path("/categories")
public class CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Category> getAll() {
        return categoryDAO.getAll();
    }

    @PermitAll
    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Category getOne(@PathParam("id") Long id) {
        return categoryDAO.get(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Category create(Category category, @QueryParam("parentId") Long parentId) {
        Category newCategory = new Category();
        newCategory.setName(category.getName());
        if (parentId != null) {
            newCategory.setParentId(parentId);
        }
        newCategory = categoryDAO.create(newCategory);
        return newCategory;
    }

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Category update(Category category, @QueryParam("parentId") Long parentId) {
        categoryDAO.update(category);
        return categoryDAO.get(category.getId());
    }

    @RolesAllowed("ROLE_ADMIN")
    @DELETE
    @Path("/{id}/delete")
    public Response delete(@PathParam("id") Long id) throws Exception {
        Category category = categoryDAO.get(id);
        categoryDAO.remove(category);
        return Response.ok().build();
    }
}
