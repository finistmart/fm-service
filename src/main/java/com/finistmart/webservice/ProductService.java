package com.finistmart.webservice;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.finistmart.domain.Category;
import com.finistmart.domain.CategoryDAO;
import com.finistmart.domain.Product;
import com.finistmart.domain.ProductDAO;
import com.finistmart.domain.ProductImage;
import com.finistmart.domain.ProductImageDAO;
import com.finistmart.dto.ProductsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Path("/products")
public class ProductService {
    
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private CategoryDAO categoryDAO;
    @Autowired
    private ProductImageDAO productImageDAO;

    @PermitAll
    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Product get(@PathParam("id") Long productId) {
        return productDAO.get(productId);
    }

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductsResult getAll(@QueryParam("categoryId") Long categoryId, @QueryParam("pageNum") Integer pageNum, @QueryParam("pageSize") Integer pageSize) {
        if (pageNum == null) {
            pageNum = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Long total = productDAO.getByCategoryIdCount(categoryId);
        List<Product> results = productDAO.getByCategoryId(categoryId, pageNum, pageSize);
        return new ProductsResult(results, total, pageNum, pageSize);
    }

    @RolesAllowed("ROLE_ADMIN")
    @POST
    @Path("add")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public Product create(@QueryParam("categoryId") Long categoryId, Product product) {
        if (categoryId != null) {
            Category category = categoryDAO.get(categoryId);
            List<Category> categories = new ArrayList<>();
            categories.add(category);
            product.setCategories(categories);
        }
        product = productDAO.create(product);
        return product;
    }

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Product save(@PathParam("id") Long id,
                                Product product) {
        productDAO.update(product);
        ProductImage image = product.getImage();
        if (image != null) {
            image.setProduct(product);
            if (image.getId() != null) {
                productImageDAO.update(image);
            } else {
                productImageDAO.create(image);
            }
        }
        return get(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/setcategory")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Product save(@PathParam("id") Long id,
                        Category category) {
        Product product = productDAO.get(id);

        product.setCategories(new ArrayList<Category>() {{
            add(category);
        }});
        try {
            productDAO.update(product);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return get(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @DELETE
    @Path("/{id}/delete")
    public Response delete(@QueryParam("id") Long id) throws Exception {
        Product product = productDAO.get(id);
        productDAO.remove(product);
        return Response.ok().build();
    }
}
