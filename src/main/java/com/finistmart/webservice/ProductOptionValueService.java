package com.finistmart.webservice;

import com.finistmart.domain.InfoField;
import com.finistmart.domain.InfoFieldDAO;
import com.finistmart.domain.Product;
import com.finistmart.domain.ProductDAO;
import com.finistmart.domain.ProductOptionSet;
import com.finistmart.domain.ProductOptionSetDAO;
import com.finistmart.domain.ProductOptionValue;
import com.finistmart.domain.ProductOptionValueDAO;
import com.finistmart.dto.OptionValueAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Component
@Path("/optionvalues")
public class ProductOptionValueService {
    
    @Autowired
    private ProductOptionValueDAO productOptionValueDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ProductOptionSetDAO productOptionSetDAO;

    @Autowired
    private InfoFieldDAO infoFieldDAO;

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductOptionValue> getAll(@QueryParam("productId") Long productId) {
        return productOptionValueDAO.getByProductId(productId);
    }

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/byoptionset")
    public List<ProductOptionValue> getByOptionSet(@QueryParam("optionSetId") Long optionSetId) {
        return productOptionValueDAO.getByOptionSet(optionSetId);
    }

    @PermitAll
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductOptionValue create(OptionValueAdd optionValueAdd) {
        ProductOptionValue value = new ProductOptionValue();
        ProductOptionSet optionSet = productOptionSetDAO.get(optionValueAdd.getOptionSetId());
        value.setProductOptionSet(optionSet);
        InfoField infoField = infoFieldDAO.get(optionValueAdd.getInfoFieldId());
        value.setInfoField(infoField);
        value.setValue(optionValueAdd.getValue());
        return productOptionValueDAO.create(value);
    }

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductOptionValue save(@PathParam("id") Long id,
                                 ProductOptionValue optionValue) {
        productOptionValueDAO.update(optionValue);
        return optionValue;
    }

    @RolesAllowed("ROLE_ADMIN")
    @DELETE
    @Path("/{id}/delete")
    public Response delete(@PathParam("id") Long id) throws Exception {
        ProductOptionValue value = productOptionValueDAO.get(id);
        productOptionValueDAO.remove(value);
        return Response.ok().build();
    }
}
