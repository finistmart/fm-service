package com.finistmart.webservice;

import com.finistmart.domain.Category;
import com.finistmart.domain.CategoryDAO;
import com.finistmart.domain.Product;
import com.finistmart.domain.ProductDAO;
import com.finistmart.domain.ProductImage;
import com.finistmart.domain.ProductImageDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@Component
@Path("/productimages")
public class ProductImageService {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private ProductImageDAO productImageDAO;

    @PermitAll
    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductImage getOne(@PathParam("id") Long id) {
        return productImageDAO.get(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @POST
    @Path("add")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    public ProductImage create(@QueryParam("productId") Long productId, ProductImage productImage) {
        if (productId != null) {
            Product product = productDAO.get(productId);
            productImage.setProduct(product);
        }
        productImage = productImageDAO.create(productImage);
        return productImage;
    }




    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductImage save(@QueryParam("productId") Long productId, ProductImage productImage) {
        if (productId != null) {
            Product product = productDAO.get(productId);
            productImage.setProduct(product);
        }
        productImage = productImageDAO.update(productImage);
        return productImage;
    }

    @RolesAllowed("ROLE_ADMIN")
    @GET
    @Path("/{id}/delete")
    public Response delete(@QueryParam("id") Long id) throws Exception {
        Product product = productDAO.get(id);
        productDAO.remove(product);
        return Response.ok().build();
    }
}
