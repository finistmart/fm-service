package com.finistmart.webservice;

import com.finistmart.domain.Order;
import com.finistmart.domain.OrderDAO;

import com.finistmart.dto.OrdersAdd;
import com.finistmart.dto.OrdersEdit;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component
@Path("/orders")
public class OrderService {
    
    @Autowired
    private OrderDAO orderDAO;

    private Mapper mapper = DozerBeanMapperBuilder.buildDefault();

    public Mapper getMapper() {
        return mapper;
    }


    @PermitAll
    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Order getOne(@PathParam("id") Long id) {
        return orderDAO.get(id);
    }

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> getAll(@QueryParam("show_case_id") Long showCaseId, @QueryParam("app_ident") String appIdent) {
        return orderDAO.getAll(showCaseId, appIdent);
    }

    @RolesAllowed("ROLE_ADMIN")
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> create(OrdersAdd ordersAdd) {
        Order newOrder = mapper.map(ordersAdd, Order.class);
        newOrder.setStatus("NEW");
        newOrder.setCreated(LocalDateTime.now());
        newOrder.setAppType("portal");
        orderDAO.create(newOrder);
        return getAll(null, null);
    }

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Order save(OrdersEdit ordersEdit) {
        Order order = orderDAO.get(ordersEdit.getId());
        mapper.map(ordersEdit, order);
        order.setUpdated(LocalDateTime.now());
        orderDAO.update(order);
        return order;
    }

    @RolesAllowed("ROLE_ADMIN")
    @GET
    @Path("/{id}/delete")
    public Response delete(@QueryParam("id") Long id) throws Exception {
        Order order = orderDAO.get(id);
        orderDAO.remove(order);
        return Response.temporaryRedirect(new URI("/orders")).build();
    }
}
