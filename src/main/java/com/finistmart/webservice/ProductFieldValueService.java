package com.finistmart.webservice;

import com.finistmart.domain.InfoField;
import com.finistmart.domain.InfoFieldDAO;
import com.finistmart.domain.Product;
import com.finistmart.domain.ProductDAO;
import com.finistmart.domain.ProductFieldValue;
import com.finistmart.domain.ProductFieldValueDAO;

import com.finistmart.domain.ProductOptionValue;
import com.finistmart.dto.ProductValueAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Component
@Path("/fieldvalues")
public class ProductFieldValueService {
    
    @Autowired
    private ProductFieldValueDAO productFieldValueDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private InfoFieldDAO infoFieldDAO;

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductFieldValue save(@PathParam("id") Long id,
                                 ProductFieldValue fieldValue) {
        ProductFieldValue originalFieldValue = productFieldValueDAO.get(id);
        originalFieldValue.setValue(fieldValue.getValue());
        productFieldValueDAO.update(originalFieldValue);
        return fieldValue;
    }

    @RolesAllowed("ROLE_ADMIN")
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductFieldValue create(ProductValueAdd productValueAdd) {
        ProductFieldValue value = new ProductFieldValue();
        Product product = productDAO.get(productValueAdd.getProductId());
        value.setProduct(product);
        InfoField infoField = infoFieldDAO.get(productValueAdd.getInfoFieldId());
        value.setInfoField(infoField);
        value.setValue(productValueAdd.getValue());
        return productFieldValueDAO.create(value);
    }

    @RolesAllowed("ROLE_ADMIN")
    @DELETE
    @Path("/{id}/delete")
    public Response delete(@PathParam("id") Long id) throws Exception {
        ProductFieldValue value = productFieldValueDAO.get(id);
        productFieldValueDAO.remove(value);
        return Response.ok().build();
    }
}
