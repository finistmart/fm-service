package com.finistmart.webservice;

import com.finistmart.domain.InfoField;
import com.finistmart.domain.InfoFieldDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Component
@Path("/infofields")
public class InfoFieldService {
    
    @Autowired
    private InfoFieldDAO infoFieldDAO;

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<InfoField> getAll() {
        return infoFieldDAO.getAll();
    }

}
