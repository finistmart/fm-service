package com.finistmart.webservice;

import com.finistmart.domain.OptionSetImage;
import com.finistmart.domain.OptionSetImageDAO;
import com.finistmart.domain.Product;
import com.finistmart.domain.ProductDAO;
import com.finistmart.domain.ProductOption;
import com.finistmart.domain.ProductOptionDAO;
import com.finistmart.domain.ProductOptionSet;
import com.finistmart.domain.ProductOptionSetDAO;
import com.finistmart.domain.ProductOptionValue;
import com.finistmart.domain.ProductOptionValueDAO;
import com.finistmart.dto.ProductOptionSetAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Component
@Path("/optionsets")
public class OptionSetService {
    

    @Autowired
    private OptionSetImageDAO optionSetImageDAO;

    @Autowired
    private ProductOptionValueDAO productOptionValueDAO;

    @Autowired
    private ProductOptionSetDAO optionSetDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ProductOptionDAO optionDAO;

    @PermitAll
    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductOptionSet get(@PathParam("id") Long id) {
        return optionSetDAO.get(id);
    }


    @RolesAllowed("ROLE_ADMIN")
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductOptionSet create(ProductOptionSetAdd optionSetAdd) {
        ProductOptionSet optionSet = new ProductOptionSet();
        optionSet.setName(optionSetAdd.getName());
        ProductOption option = optionDAO.get(optionSetAdd.getProductOptionId());
        optionSet.setProductOption(option);
        Product product = productDAO.get(optionSetAdd.getProductId());
        optionSet.setProduct(product);
        return optionSetDAO.create(optionSet);
    }

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductOptionSet save(@PathParam("id") Long id,
                                ProductOptionSet optionSet) {
        ProductOptionSet origOptionSet = get(id);
        optionSet.setProduct(origOptionSet.getProduct());
        optionSetDAO.update(optionSet);
        OptionSetImage image = optionSet.getImage();
        if (image != null) {
            image.setOptionSet(optionSet);
            if (image.getId() != null) {
                optionSetImageDAO.update(image);
            } else {
                optionSetImageDAO.create(image);
            }
        }
        return optionSet;
    }

    @RolesAllowed("ROLE_ADMIN")
    @DELETE
    @Path("/{id}/delete")
    public Response delete(@PathParam("id") Long id) throws Exception {
        ProductOptionSet optionSet = optionSetDAO.get(id);
        if (optionSet.getImage() != null) {
            optionSetImageDAO.delete(optionSet.getImage());
        }
        if (optionSet.getProduct() != null) {
            List<ProductOptionValue> optionValues = productOptionValueDAO.getByProductId(optionSet.getProduct().getId());
            for (ProductOptionValue value : optionValues) {
                productOptionValueDAO.remove(value);
            }
        }
        optionSetDAO.delete(optionSet);
        return Response.ok("{}").build();
    }
}
