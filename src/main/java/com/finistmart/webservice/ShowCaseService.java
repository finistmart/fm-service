package com.finistmart.webservice;

import com.finistmart.domain.ShowCase;
import com.finistmart.domain.ShowCaseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;


@Component
@Path("/showcases")
public class ShowCaseService {
    
    @Autowired
    private ShowCaseDAO showCaseDAO;

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ShowCase> getAll() {
        return showCaseDAO.getAll();
    }

    @RolesAllowed("ROLE_ADMIN")
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ShowCase> create(@QueryParam("name") String name) {
        ShowCase newShowCase = new ShowCase();
        newShowCase.setName(name);
        showCaseDAO.create(newShowCase);
        return getAll();
    }

    @RolesAllowed("ROLE_ADMIN")
    @PUT
    @Path("/{id}/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ShowCase> APPLICATION_JSON(@PathParam("id") Long id,
                                @QueryParam("name") String name) {
        ShowCase showCase = new ShowCase();
        showCase.setId(id);
        showCase.setName(name);
        showCaseDAO.update(showCase);
        return getAll();
    }

    @RolesAllowed("ROLE_ADMIN")
    @GET
    @Path("/{id}/delete")
    public Response delete(@QueryParam("id") Long id) throws Exception {
        ShowCase showCase = showCaseDAO.get(id);
        showCaseDAO.remove(showCase);
        return Response.temporaryRedirect(new URI("/products")).build();
    }
}
