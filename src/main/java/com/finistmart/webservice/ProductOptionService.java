package com.finistmart.webservice;

import com.finistmart.domain.ProductOption;
import com.finistmart.domain.ProductOptionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Component
@Path("/productoptions")
public class ProductOptionService {
    
    @Autowired
    private ProductOptionDAO productOptionDAO;

    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductOption> getAll() {
        return productOptionDAO.getAll();
    }

}
